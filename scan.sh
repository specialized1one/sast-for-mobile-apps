#!/bin/sh

#Define the API key to use for API interaction
MOBSF_API_KEY=4824a7dfb3cd6aa0da29c9ae500cc6cee132112010a0c10690e27da0c7478e7e

#Upload the file
response=`curl -F 'file=@./allsafe.apk' http://localhost:8000/api/v1/upload -H "Authorization:$MOBSF_API_KEY"`
SCAN_TYPE=`echo $response | jq '.scan_type' | sed 's/"//g'`
FILE_NAME=`echo $response | jq '.file_name' | sed 's/"//g'`
HASH=`echo $response | jq '.hash' | sed 's/"//g'`

#Perform the scan
scan_response=`curl -X POST --url http://localhost:8000/api/v1/scan --data "scan_type=$SCAN_TYPE&file_name=$FILE_NAME&hash=$HASH" -H "Authorization:$MOBSF_API_KEY"`
echo $scan_response >> ./reports/report.json

